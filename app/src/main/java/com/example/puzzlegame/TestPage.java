package com.example.puzzlegame;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.puzzlegame.Page.PuzzlePage;
import com.example.puzzlegame.Utils.PhotoUtil;
//作为测试用，非正式内容
public class TestPage extends Activity {
    private TextView mBtn;
    private TextView mPath;
    private TextView mUri;
    private ImageView mImage;
    private final long waiting_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        initView();

        initEvent();



    }

    private void initEvent() {
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //调用相册
                startActivityForResult(new Intent(Intent.ACTION_PICK).setType("image/*"),0);
            }
        });
    }

    private void initView() {
        mBtn = (TextView) findViewById(R.id.btn);
        mPath = (TextView) findViewById(R.id.tv_path);
        mUri = (TextView) findViewById(R.id.tv_uri);
        mImage = (ImageView) findViewById(R.id.iv_bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //应该是四大组件的内容提供者对应的内功接收者 ，必调方法
        ContentResolver contentResolver = getContentResolver();
        Bitmap bitmap = null;
        try {
            if(requestCode == 0){
                //图片uri
                Uri uri = data.getData();
                Log.e("tag","uri = "+uri);
                mUri.setText("Uri地址："+uri);
                //返回图片
                mImage.setImageURI(uri);
                //使用工具类获取绝对路径
                String  path= PhotoUtil.getFilePathFromContentUri(uri, contentResolver);
                Log.e("tag","path = "+path);
                mPath.setText("Path地址："+path);

                String imagePath2 = path;
/*
                Intent intent2=getIntent();
                Bundle bundle=intent2.getExtras();
                String imagePath=bundle.getString("imagePath");
*/




                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {  //使用handler的postDelayed实现延时跳转

                    public void run() {
                        Intent intent=getIntent();
                        Bundle bundle=intent.getExtras();
                        String imagePath1=bundle.getString("imagePath1");
                        Intent intent1 = new Intent(TestPage.this, PuzzlePage.class);
                        intent1.putExtra("imagePath1", imagePath1);
                        intent1.putExtra("imagePath1",imagePath2);
                        startActivity(intent1);
                        finish();
                    }
                }, waiting_LENGTH);



            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


