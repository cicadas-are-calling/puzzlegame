package com.example.puzzlegame.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * 文件Uri转路径
 */
public class FileUtils {

    //Android获取本地文件的真实路径，content类型转为file类型
    //兼容不同的机型获取到的文件路径类型，以防异常情况发生
    @SuppressLint("Recycle")
    public static String getPath(Context context, Uri uri) {    //引用别人封装的解析Uri的方法将它们统一转化成File路径
/*
 * 根据Uri返回文件绝对路径
 * 兼容了file:///开头的 和 content://开头的情况
 */


        if ("content".equalsIgnoreCase(uri.getScheme())) {// 如果是 content 类型的 Uri
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection,null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {

            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }
}
