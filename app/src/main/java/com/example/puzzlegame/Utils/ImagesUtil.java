package com.example.puzzlegame.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;

import com.example.puzzlegame.R;

import java.util.ArrayList;
import java.util.List;

//图片处理，切割类
public class ImagesUtil {
    public static ImgBean imgBean;
    public static int itemWidth = 0;
    public static int itemHeight = 0;

    private static Paint mPaint;
    public static float scale ;

    ////对传入图片进行处理，分割
    public static void createInitBitmaps(Context context, int type, Bitmap picSelected){
        GameUtil.mImgBeans.clear();

        Bitmap bitmap = null;
        //获取所选图片的长宽
        int width = picSelected.getWidth();
        int height = picSelected.getHeight();
        scale = (float)width/height;    //计算（压缩）比例
        int screenWidth = ScreenUtil.getScreenWidth(context);

        picSelected = resizeBitmap(screenWidth, screenWidth/scale, picSelected);//根据手机屏幕宽度来对图片进行处理

        List<Bitmap> bitmapItems = new ArrayList<>();
        itemWidth = picSelected.getWidth()/type;    //type范围：3,4,5
        itemHeight = picSelected.getHeight()/type;
        for (int i=1; i<=type; i++){
            for (int j=1; j<=type; j++){
                bitmap = Bitmap.createBitmap(
                        picSelected,
                        (j-1)*itemWidth,    //裁剪x方向的起始位置
                        (i-1)*itemHeight,
                        itemWidth,          //裁剪宽度
                        itemHeight          //裁剪高度
                );
                bitmap = getRoundedCornerBitmap(bitmap);//获得圆角图片

                bitmapItems.add(bitmap);//加入集合中
                imgBean = new ImgBean(
                        (i-1)*type+j,//1,2,3,4....9记录id
                        (i-1)*type+j,
                        false,
                        bitmap
                );
                GameUtil.mImgBeans.add(imgBean);//封装成单元格
            }
        }
        // 设置最后一个为空Item
        GameUtil.mLastBitmap = bitmapItems.get(type*type-1);//最后一块图
        bitmapItems.remove(type*type-1);//移除最后一块图
        GameUtil.mImgBeans.remove(type*type-1);
        Bitmap blankBitmap = BitmapFactory.decodeResource(
                context.getResources(), R.drawable.blank
        );
        blankBitmap = Bitmap.createBitmap(
                blankBitmap, 0, 0, itemWidth, itemHeight);  //生成空白块
        bitmapItems.add(blankBitmap);//把空白块加入List
        GameUtil.mImgBeans.add(new ImgBean(type*type, 0, false, blankBitmap));//空白图片添加（代号为0）
        GameUtil.mBlankImgBean = GameUtil.mImgBeans.get(type*type-1);
    }

    public static Bitmap resizeBitmap(float newWidth, float newHeight, Bitmap bitmap){
        Matrix matrix = new Matrix();
        matrix.postScale(         //获取想要缩放的matrix
                newWidth/bitmap.getWidth(),
                newHeight/bitmap.getHeight()
        );

        Bitmap newBitmap = Bitmap.createBitmap(      //获取新的bitmap
                bitmap, 0, 0,
                bitmap.getWidth(),
                bitmap.getHeight(),
                matrix, true);
        return newBitmap;
    }



     //获得圆角图片的方法
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap mBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mBitmap);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);

/*      参数说明s
        rect：RectF对象。
        rx：x方向上的圆角半径。
        ry：y方向上的圆角半径。
        paint：绘制时所使用的画笔。*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawRoundRect(0, 0, bitmap.getWidth(), bitmap.getHeight(),
                    20, 20, mPaint);
        }

        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN)); //取两层绘制交集。显示上层
        canvas.drawBitmap(bitmap, 0, 0, mPaint);

        return mBitmap;
    }









   /* public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight)
    {
        // 第一次解析将inJustDecodeBounds设置为true，来获取图片大小
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        // 调用上面定义的方法计算inSampleSize值
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // 使用获取到的inSampleSize值再次解析图片
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);

    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                                             int reqHeight)
    {
        // 源图片的高度和宽度
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // 计算出实际宽高和目标宽高的比率
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // 选择宽和高中最小的比率作为inSampleSize的值，这样可以保证最终图片的宽和高
            // 一定都会大于等于目标的宽和高。
            inSampleSize = Math.min(heightRatio, widthRatio);
        }
        return inSampleSize;
    }*/

}
