package com.example.puzzlegame.view;

public interface GameInterface {
    void isSuccessful();
    void addStep();
}
