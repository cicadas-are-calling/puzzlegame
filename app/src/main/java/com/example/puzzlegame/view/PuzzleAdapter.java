package com.example.puzzlegame.view;


import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


import com.example.puzzlegame.R;
import com.example.puzzlegame.Utils.GameUtil;
import com.example.puzzlegame.Utils.ImgBean;

import java.util.List;
/*ListView,GridView所展示数据的格式则是有一定的要求的。数据适配器正是建立了数据源与组件之间的适配关系，
 将数据源转换为视图组件能够显示的数据格式，从而将数据的来源与数据的显示进行解耦，降低程序的耦合性*/
public class PuzzleAdapter extends BaseAdapter{
    private Context context;
    private List<ImgBean> picList;  //数据源

    //关联数据源(picList)与适配器界面对象（context）
    public PuzzleAdapter(Context context, List<ImgBean> picList){
        super();
        this.context = context;
        this.picList = picList;
    }

    //继承BaseAdapter类后需要重写四个方法：：getCount、getItem、getItemId、getView，如下

    @Override
    public int getCount() {//ListView等视图控件需要显示的数据数量
        if(null != GameUtil.mImgBeans)
            return GameUtil.mImgBeans.size();
        return 0;
    }

    @Override
    // 指定的索引对应的数据项
    public Object getItem(int position) {
        return GameUtil.mImgBeans.get(position);
    }

    @Override
    //指定的索引对应的数据项ID
    public long getItemId(int position) {
        return position;
    }

    @Override
    //返回每一项的显示内容
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView pic = null;

        //如果view未被实例化过，缓存池中没有对应的缓存
        if (null == convertView){
            pic = new ImageView(context);
            pic.setLayoutParams(new GridView.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT        //布局使用LayoutParams设置布局宽高自适应
            ));
            pic.setScaleType(ImageView.ScaleType.FIT_XY);
            //Scaletype决定了图片在View上显示时的样子,此处FIT_XY:不按比例缩放图片，目标是把图片塞满整个View
        } else {
            pic = (ImageView) convertView;//已经实例化就可以直接使用了
        }

        pic.setBackgroundResource(R.drawable.img_item_bg);//视图的背景设置

        pic.setImageBitmap(picList.get(position).getmBitmap());

        return pic ;
    }
}
