package com.example.puzzlegame.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.GridView;

import com.example.puzzlegame.Page.PuzzlePage;
import com.example.puzzlegame.Utils.GameUtil;
import com.example.puzzlegame.Utils.ImgBean;

public class ScrollGridView extends GridView {  //单元格移动

    public static int Gesture_Top = 1;
    public static int Gesture_RIGHT = 2;
    public static int Gesture_DOWN = 3;
    public static int Gesture_LEFT = 4;

    private boolean isReady = true ;

    private GameInterface mInterface;


    int blankPosition = -1;

    public ScrollGridView(Context context) {
        this(context, null);
    }

    public ScrollGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setPosition(int position){
        this.blankPosition = position ;
    }

    public void setInterface(GameInterface anInterface) {
        mInterface = anInterface;
    }

    public void setGesture(int gesture){    //手势
        int type = PuzzlePage.GAME_TYPE;

        int i = blankPosition / type ;
        int j = (blankPosition+1) % type ;

        if (gesture == Gesture_Top){
            if (i != (type-1)){//判断此碎片是否能被移出，以3x3为例，空格除去7,8,9这三个位置外均能 执行上滑操作
                startAnimator(blankPosition+type, "translationY");//translationY,相对于最初位置的y方向的偏移值
            }
        }
        if (gesture == Gesture_DOWN){
            if (i != 0){    //以3x3为例，空格除去1,2,3这三个位置外均能 执行下滑操作
                startAnimator(blankPosition-type, "translationY");
            }
        }
        if (gesture == Gesture_LEFT){
            if (j != 0){
                startAnimator(blankPosition+1, "translationX");//translationX,相对于最初位置的x方向的偏移值
            }
        }
        if (gesture == Gesture_RIGHT){
            if (j != 1){
                startAnimator(blankPosition-1, "translationX");
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false ;
    }
    //触摸事件与动画
    private void startAnimator(final int childId, String type) {
        if (!isReady){
            return;
        }
        getChildAt(blankPosition).setVisibility(INVISIBLE);//getChildAt根据参数返回对应view内容，并且设置为不可见
        float distance = 0f;
        if (type == "translationX"){
            distance = getChildAt(blankPosition).getX() - getChildAt(childId).getX();
        }
        if (type == "translationY"){
            distance = getChildAt(blankPosition).getY() - getChildAt(childId).getY();
        }

        final int x = (int) getChildAt(childId).getTranslationX();
        final int y = (int) getChildAt(childId).getTranslationY();

        //ObjectAnimator.ofFloat动画
        ObjectAnimator animator = ObjectAnimator.ofFloat(getChildAt(childId), type,
                0, distance);   //distance为移动像素
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                isReady = false;
                mInterface.addStep();
            }

            @Override
            public void onAnimationEnd(Animator animation) {    //演示动画结束后要进行数据更新
                isReady = true;

                GameUtil.swapItems(GameUtil.mImgBeans.get(childId),
                        GameUtil.mBlankImgBean);    //滑动操作结束后交换空格和item格子

                PuzzlePage.adapter.notifyDataSetChanged();//更新UI

                getChildAt(childId).setTranslationX(x);
                getChildAt(childId).setTranslationY(y);

                getChildAt(blankPosition).setVisibility(VISIBLE);//空白视图 设置为可见

                blankPosition = childId ;//更新空格id

                if (isSuccess()){
                    mInterface.isSuccessful();
                }
            }
        });
        animator.setDuration(300);
        animator.start();
    }
    //是否拼图成功，即当前图片Item的ID与初始状态下图片的ID是否相同。
    private boolean isSuccess(){
        for (ImgBean tempBean : GameUtil.mImgBeans){
            Log.d("TAG","PuzzlePage.GAME_TYPE"+PuzzlePage.GAME_TYPE);
            if(tempBean.getmBitmapId() != 0 && tempBean.getmItemId() == tempBean.getmBitmapId()){
                continue;   //空格此处无须比较
            } else if (tempBean.getmBitmapId() == 0 && (tempBean.getmItemId() == (PuzzlePage.GAME_TYPE * PuzzlePage.GAME_TYPE))){
                continue;   //空白格与原图第九片重合
            } else {
                return false;
            }
        }
        return true;
    }
}