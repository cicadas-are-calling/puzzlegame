package com.example.puzzlegame.Page;

import android.annotation.SuppressLint;
import android.app.Activity;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.puzzlegame.R;
import com.example.puzzlegame.Utils.GameUtil;
import com.example.puzzlegame.Utils.ImagesUtil;
import com.example.puzzlegame.Utils.ScreenUtil;
import com.example.puzzlegame.Utils.SizeHelper;
import com.example.puzzlegame.WelcomePage;
import com.example.puzzlegame.view.GameInterface;
import com.example.puzzlegame.view.PuzzleAdapter;
import com.example.puzzlegame.view.ScrollGridView;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import static android.R.id.message;

public class PuzzlePage extends Activity {

    public static int GAME_TYPE = 3;
    public static boolean flag = true;//用来区分休闲模式的
    private int screenWidth ;

    private ScrollGridView gridView;
    private TextView timer, stepCount ;

    private ImageButton look ;

    private Bitmap mBitmap;

    private int time = -1 ;
    private int step = 0 ;

    private GestureDetector mGestureDetector ;
    public static PuzzleAdapter adapter;

    private SoundPool spool;
    private int id;

    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mWindowLayoutParams;
    private ImageView mImageView;

    private MediaPlayer playerbg;
    private int playerbgstate = 0;

    private AudioManager audio;

    @SuppressLint("HandlerLeak")
    public Handler mHandler = new Handler(){
        //@SuppressLint("SetTextI18n")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    Log.d("successful", "true");
                    timer.setEnabled(false);

                    //拼图成功音效
                    MediaPlayer mMediaPlayer;//MediaPlayer类是同步执行的只能一个文件一个文件的播放
                    mMediaPlayer=MediaPlayer.create(PuzzlePage.this, R.raw.cheer);
                    mMediaPlayer.start();


                    new AlertDialog.Builder(PuzzlePage.this).setMessage("恭喜您拼图成功，您用了"+ time + "秒，" + step + "步").setPositiveButton("确定", (dialog, id) -> {
                        Intent intent = new Intent(PuzzlePage.this, WelcomePage.class);//跳回主页
                        startActivity(intent);
                    }).setCancelable(false).show();
                    break;
                case 2:
                    timer.setText(time + "s");
                    break;
                case 3:
                    stepCount.setText(step + "");
                    break;
                case 4:
                    time = -1;
                    task.cancel();
                    startTimer();
                    step = 0;
                    stepCount.setText(step + "");
                    initGridView(i.getExtras().getString("imagePath"));
                    break;

                case 5: //拼图步数过多失败
                    Log.d("false", "true");
                    timer.setEnabled(false);


                    new AlertDialog.Builder(PuzzlePage.this).setMessage("哎呀，是不是太难了，您可以尝试切换难度~").setPositiveButton("确定", (dialog, id) -> {
                        Intent intent = new Intent(PuzzlePage.this, WelcomePage.class);//跳回主页
                        startActivity(intent);
                    }).setCancelable(false).show();
                    break;

            }
        }
    };

    private Timer mTimer ;
    private TimerTask task ;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moving_recording);
        SizeHelper.prepare(this);

        mWindowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);//获取窗口服务
        audio = (AudioManager) getSystemService(Service.AUDIO_SERVICE); //获取AudioManage引用
        startTimer();
        initView();
        id = initSoundpool();

        i = this.getIntent();//获得intent内容

        initGridView(i.getExtras().getString("imagePath"));//获取图像（路径）并处理
/*
        if(i != null) {

            if ((Objects.requireNonNull(i.getExtras())).getString("imagePath1") != null && i.getExtras().getString("imagePath2") == null) {
                initGridView(Objects.requireNonNull(i.getExtras()).getString("imagePath1"));//获取图像（路径）并处理
            }

            if ((i.getExtras()).getString("imagePath2") != null && i.getExtras().getString("imagePath1") == null) {
                    initGridView(Objects.requireNonNull(i.getExtras()).getString("imagePath2"));//获取图像（路径）并处理
            }

        }
*/



        playerbg = MediaPlayer.create(this, R.raw.bgm);
        playerbg.setLooping(true);
        playerbg.start();
        playerbgstate = 1;
    }

    private void startTimer() { //开始计时器
        time = -1;
        mTimer = new Timer(true);

        task = new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = 2;
                time++;
                mHandler.sendMessage(message);//更新时间
            }
        };
        mTimer.schedule(task, 0, 1000);//定时器间隔1s调用一次run方法
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        mGestureDetector = new GestureDetector(this, listener);//手势监听检测
        screenWidth = ScreenUtil.getScreenWidth(this);
        look = (ImageButton) findViewById(R.id.help1);  //绑定“提示”按钮
        look.setOnTouchListener((v, event) -> {
            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN://检测按下
                    creatMirrorImage();
                    break;
                case MotionEvent.ACTION_UP://检测抬起
                    deleteMirrorImage();
                    break;
            }
            return false;
        });

        timer = (TextView) findViewById(R.id.timer) ;
        stepCount = (TextView) findViewById(R.id.step) ;

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.titlebar);   //顶端条

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                SizeHelper.fromPx(74));//设置子视图(顶端条)的宽，高
        layout.setLayoutParams(params);
    }

    private int initSoundpool() {
        //Sdk版本>=21时使用下面的方法
        if(Build.VERSION.SDK_INT>=21){
            SoundPool.Builder builder=new SoundPool.Builder();//SoundPool类支持同时播放多个音效
            //设置最多容纳的流数
            builder.setMaxStreams(2);
            AudioAttributes.Builder attrBuilder=new AudioAttributes.Builder();
            attrBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
            builder.setAudioAttributes(attrBuilder.build());
            spool=builder.build();
        }else{

            spool=new SoundPool(2,AudioManager.STREAM_MUSIC, 0);


        }
        //加载滑动碎片时的音频文件，返回音频文件的id
        int id=spool.load(getApplicationContext(),R.raw.slip,1);

        return id;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }
    //手势识别器
    GestureDetector.OnGestureListener listener = new
            GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2,
                                       float velocityX, float velocityY) {
                    //e1为(手势起点)第一次按下坐标，e2为（手势终点）手指离开屏幕时坐标
                    //安卓的坐标原点是在左上角的，向下是y正轴，向右是x正轴
                    float x = e2.getX() - e1.getX() ;
                    float y = e2.getY() - e1.getY() ;

                    float x_abs = Math.abs(x);//返回绝对值
                    float y_abs = Math.abs(y);

                    int gesture = 0;
                    if (x_abs >= y_abs){    //如果x轴上的位移计算结果是比y轴上大
                        if (x_abs >= screenWidth / 5){  //如果滑动距离过短则判定无效
                            if (x>0){
                                gesture = ScrollGridView.Gesture_RIGHT; //水平向右移动
                            } else {
                                gesture = ScrollGridView.Gesture_LEFT;  //水平向左滑动
                            }
                        }
                    } else {
                        if (y_abs >= screenWidth / 5){
                            if (y > 0){
                                gesture = ScrollGridView.Gesture_DOWN;  //向下滑动
                            } else {
                                gesture = ScrollGridView.Gesture_Top;   //向上滑动
                            }
                        }
                    }

                    gridView.setGesture(gesture);
                    spool.play(id, 1, 1, 0, 0, 1);

                    return true;
                }
            };
    //接受图片路径后对图片进行处理加工
    private void initGridView(String imagePath) {
        gridView = (ScrollGridView) findViewById(R.id.grid_list);

        mBitmap = BitmapFactory.decodeFile(imagePath);//从文件中解析出一个位图对象，返回的位图是不可改变的。方法参数为位图文件的路径。返回的位图默认为不可改变的。

        ImagesUtil.createInitBitmaps(this, GAME_TYPE, mBitmap);//对传入图片进行处理，分割
        GameUtil.getPuzzleGenertor();//对分割完的图片进行打乱处理

        adapter = new PuzzleAdapter(this, GameUtil.mImgBeans);
        gridView.setNumColumns(GAME_TYPE);
        gridView.setAdapter(adapter);
//        Log.d("TAG","123456");
        gridView.setPosition(GameUtil.blankPosition);
        gridView.setInterface(new GameInterface() {
            @Override
            public void isSuccessful() {
                Message message = new Message();
                message.what = 1;
                mHandler.sendMessage(message);

            }

            @Override
            public void addStep() {
                addStepCount();
                Log.d("TAG","操作x1");
            }
        });
    }

    private void addStepCount(){
        step ++ ;

        //在这里穿插一个如果步数过多拼图失败的判定设置
/*        Intent intent = getIntent();
        boolean modelFlag = intent.getBooleanExtra("modelFlag",true);*/

        if(flag) { //非休闲模式才执行
            switch (GAME_TYPE) {
                case 3:
                    if(step>=128){
                        Message message = new Message();
                        message.what = 5;
                        mHandler.sendMessage(message);
                    }

                    break;
                case 4:
                    if(step>=256){
                        Message message = new Message();
                        message.what = 5;
                        mHandler.sendMessage(message);
                    }

                    break;

                case 5:
                    if(step>=512){
                        Message message = new Message();
                        message.what = 5;
                        mHandler.sendMessage(message);
                    }

                    break;

            }
        }


        Message message = new Message();
        message.what = 3;
        mHandler.sendMessage(message);
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.help:
                AlertDialog.Builder builder1 = new AlertDialog.Builder(PuzzlePage.this);
                builder1.setMessage("游戏图标操作功能依次为背景音乐控制、原图帮助、难度选择、生成新游戏，" +
                        "其中原图帮助需长按图标方能显示图片，松开后原图消失。"+
                        "进入打乱的图片页面，将空白处周围的图片移向空白处，继续这样的操作，直到拼完。"
                );
                builder1.setTitle("游戏帮助");

                builder1.create().show();
                break;
            case R.id.fanhui:
                new AlertDialog.Builder(this).setTitle("确定要重新生成游戏么？")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restartGames();
                            }
                        })
                        .setNegativeButton("返回",null).show();
                break;
            case R.id.nosilence://静音
                ImageButton bgm;
                bgm = (ImageButton)findViewById(R.id.nosilence);//设置 对应的静音按钮
                if(playerbgstate == 1){     //如果此时是有背景音乐则把图标切换为静音资源
                    bgm.setBackgroundResource(R.drawable.silence);
                    //bgm.setBackgroundDrawable(getResources().getDrawable(R.drawable.silence));
                    playerbgstate = -1;
                    playerbg.pause();//暂停播放
                }else if(playerbgstate == -1){
                    bgm.setBackgroundResource(R.drawable.nosilence);//如果点击按钮之前是暂定播放（静音）状态 则更新图标
                    playerbgstate = 1;
                    playerbg.start();//开始播放
                }

                break;
            case R.id.difficulty://游戏内难度切换
                AlertDialog.Builder builder = new AlertDialog.Builder(PuzzlePage.this);
                builder.setTitle("难度选择：");
                builder.setSingleChoiceItems(new String[]{"3X3", "4X4", "5X5"}, 0,
                        (dialog, which) -> {
                            switch (which){
                                case 0:
                                    PuzzlePage.GAME_TYPE = 3;
                                    break;
                                case 1:
                                    PuzzlePage.GAME_TYPE = 4;
                                    break;
                                case 2:
                                    PuzzlePage.GAME_TYPE = 5;
                                    break;
                            }
                        }).setPositiveButton("确定", (dialog, which) -> {
                            Message message = new Message();
                            message.what = 4;
                            mHandler.sendMessage(message);
                        }).show();



                break;
            case R.id.left_btn:     //离开，返回主界面
                if(playerbg != null){
                    playerbg.stop();
                    playerbg.release();
                    playerbg = null;
                }
                finish();
                Intent intent = new Intent(PuzzlePage.this, WelcomePage.class);
                startActivity(intent);
                break;
        }
    }
    //图像（镜像）预览
    private void creatMirrorImage() {
        AlphaAnimation animation = new AlphaAnimation(1, 0);//从不透明到完全透明的渐变过程，改变组件的透明度
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                gridView.setAlpha(0);
            }//拼图列表nxn变为透明

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        animation.setDuration(800);//渐变0.8s内完成,(完成动画的时长)
        gridView.startAnimation(animation);

        mWindowLayoutParams = new WindowManager.LayoutParams();
        mWindowLayoutParams.format = PixelFormat.TRANSLUCENT;//设置窗口格式为半透明
        mWindowLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;   //设置窗口停靠,并不改变其大小

        mWindowLayoutParams.x = gridView.getLeft() ;
/*        x :如果忽略gravity属性，那么它表示窗口的绝对X位置。
        什么是gravity属性呢？简单地说，就是窗口如何停靠。
        当设置了 Gravity.LEFT 或 Gravity.RIGHT 之后，x值就表示到特定边的距离。*/
        mWindowLayoutParams.y = gridView.getTop() ;

        mWindowLayoutParams.width = gridView.getWidth();
        mWindowLayoutParams.height = gridView.getHeight();
/*        ndroid中可以把left相当于X轴值， top相当于Y轴值， 通过这两个值Android系统可以知道视图的绘制起点，
          再通过Wdith 和 Height 可以得到视图上下左右具体值，就可以在屏幕上绝对位置绘制视图。
        right = left + width;
        视图左侧位置  view.getLeft()；获取到的是view自身的左边到其父布局左边的距离
        视图右侧位置 view.getRight()
        视图顶部位置 view.getTop();获取到的是view自身的顶边到其父布局顶边的距离
        视图底部位置 view.getBottom();bottom = top + height;  bottom就是子view底部到父view底部的距离
        视图宽度 view.getWidth();
        视图高度 view.getHeight()
*/



        mWindowLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ;
        /*FLAG_NOT_FOCUSABLE 不能获得按键输入焦点,换句话说，它可以全屏显示，如果需要的话，可覆盖输入法窗口
          FLAG_NOT_TOUCHABLE 不接受触摸屏事件
        * */

        mImageView = new ImageView(this);
        mImageView.setImageBitmap(mBitmap);

        mWindowManager.addView(mImageView, mWindowLayoutParams);//绘制（添加）“预览”视图显示
    }
    //“关闭”图像预览，“恢复”拼图界面
    private void deleteMirrorImage() {
        if (mImageView != null) {
            AlphaAnimation animation = new AlphaAnimation(0, 1);//设置从透明变为不透明
            animation.setDuration(500);
            gridView.startAnimation(animation);
            gridView.setAlpha(1);//（显示原本内容）不透明

            mWindowManager.removeView(mImageView);  //移出“预览”视图显示
        }
    }

    private void restartGames() {
        AlphaAnimation animation = new AlphaAnimation(1, 0);//从不透明到，完全透明
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                time = -1;  //计时器重置
                task.cancel();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                addStepCount();//开始步数统计

                GameUtil.getPuzzleGenertor();//调用之前的打乱函数即可
                gridView.setPosition(GameUtil.blankPosition);
                adapter.notifyDataSetChanged(); //刷新ListView
                AlphaAnimation animation1 = new AlphaAnimation(0, 1);//从透明再到不透明
                animation1.setAnimationListener(new Animation.AnimationListener() {//新动画
                    @Override
                    public void onAnimationStart(Animation animation) {
                        startTimer();
                        step = 0;
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {}

                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                });
                animation1.setDuration(1000);
                gridView.startAnimation(animation1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        animation.setDuration(1000);
        gridView.startAnimation(animation);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
     *退出程序
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP://增大音量
                audio.adjustStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_RAISE,
                        AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN://减小音量
                audio.adjustStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_LOWER,
                        AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
                return true;
            case KeyEvent.KEYCODE_BACK://返回按钮
                if(playerbg != null){//如果有声音则停止MediaPlayer
                    playerbg.stop();
                    playerbg.release();//释放这个MediaPlayer
                    playerbg = null;
                }
                finish();
                Intent intent = new Intent(PuzzlePage.this, WelcomePage.class);
                startActivity(intent);
                return true;
        }
        return super.onKeyDown(keyCode, event);


    }

    @Override
    protected void onStart() {//该方法在 onCreate() 方法之后被调用
        super.onStart();
        Log.d("TAG","  onStart ");
    }

    @Override
    protected void onStop() {// 当 Activity 被另外一个 Activity 覆盖、失去焦点并不可见时处于 Stoped 状态
        super.onStop();
        Log.d("TAG","  onStop ");
        if(playerbg != null){
            if (playerbgstate == 1) {//若有bgm则停止bgm
                playerbgstate = -1;
                playerbg.pause();
            }
        }

    }

}