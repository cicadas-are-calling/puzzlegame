package com.example.puzzlegame.Page;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.annotation.Nullable;

import com.example.puzzlegame.R;
import com.example.puzzlegame.Utils.FileUtils;
import com.example.puzzlegame.Utils.ImagesUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//获取app自带图片
public class TakePicturesPage extends Activity {

    private ListView listview;
    private ListAdapter adapter1;
    private Bitmap bitmap;



    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_02);

        listview = (ListView) findViewById(R.id.listview);//后面listview是布局里面控件ListView的id
        final String pictures[] = new String[]{"图片1","图片2","图片3", "图片4",
                                                "图片5","图片6","图片7","图片8",
                                                "图片9","图片10","图片11","图片12"};
        final int images[] = new int[]{
                R.drawable.image1,R.drawable.image2,R.drawable.image3,R.drawable.image4,
                R.drawable.image5,R.drawable.image6,R.drawable.image7,R.drawable.image8,
                R.drawable.image9,R.drawable.image10,R.drawable.image11,R.drawable.image12

        };

        //创建list集合，存储图片
        List<Map<String,Object>> data = new ArrayList<Map<String,Object>>();
        for(int i=0; i<pictures.length ; i++){
            Map<String,Object> map = new HashMap<String,Object>();//每次循环的时候都实例化一个新的map对象，这样list在执行add方法的时候，每次都是存的不一样的map对象。
            //需要创建不同的map对象的时候，需要在循环里面进行map的创建，在外面创建map的时候会造成多个对象都相同
            //put方法存储键值对 <String类型,任意类型 >
            map.put("picture", pictures[i]);//存图片对应序号
            map.put("icon", images[i]);//存入image1这些图片
            data.add(map);//再把map对象都添加到List集合中
        }
 /*  ListView所展示数据的格式则是有一定的要求的。数据适配器正是建立了数据源与ListView之间的适配关系，
        将数据源转换为ListView能够显示的数据格式，从而将数据的来源与数据的显示进行解耦，降低程序的耦合性*/

//SimpleAdapter(context, data, resource, from, to)
//- context：程序环境信息，即上下文，一般用this
//- data：一般用map类型，（键值对
//- resource：列表项的布局文件id，也就是给适配器准备的listxml
//- from：map中的键名，注意是一对
//- to：绑定视图中ImageView和TextView 二者组件对应的id
        adapter1 = new SimpleAdapter(TakePicturesPage.this, data, R.layout.listxml, new String[]{"icon","picture"}, new int[]{R.id.image1,R.id.textview1});
        listview.setAdapter(adapter1);  //setAdapter() 设置数据适配器

        //setOnItemClickListener() 设置每项点击事件监听
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){


            /* (non-Javadoc)
             * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
             *列表获取点击事件
             */


         /*
        arg0，即parent：相当于listview 适配器的一个指针，可以通过它来获得适配器里装着的一切东西

        arg1:是当前item的view，通过它可以获得该项中的各个组件。
        例如:TextView text = view.findViewById(R.id.text);

        arg2：是当前mineTravelList的positioin。这个id根据你在适配器中的写法可以自己定义。
        例如：MineTravelList travel = mineTravel.get(position);

        arg3：是当前的item在listView中的相对位置id
         */


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageView image =(ImageView) view.findViewById(R.id.image1);//绑定对应的ImageView控件


/*
                Intent intent2 = getIntent();//测试用：接受路径并处理图片防止内存溢出（OOM）
                String path2 = intent2.getStringExtra("picturePath");
                bitmap =  ImagesUtil.decodeSampledBitmapFromResource(path2, 400, 400);

*/

                bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();//把ImageView转换成Bitmap，（可以方便操作图片，尤其是切割）
                String imagePath = FileUtils.getPath(TakePicturesPage.this, Objects.requireNonNull(bitmap2uri(TakePicturesPage.this, bitmap)));
                Intent intent2 = new Intent(TakePicturesPage.this, PuzzlePage.class);//跳转到拼图模块
                intent2.putExtra("imagePath", imagePath);//键值对存储，需要用getExtra来获取


/*               boolean modelFlag = intent2.getBooleanExtra("modelFlag",true);
                intent2.putExtra("modelFlag", modelFlag);*/
                startActivity(intent2);
                finish();//结束当前活动

            }

        });
    }





    //Bitmap转uri
    public static Uri bitmap2uri(Context c, Bitmap b) {
        //用时间戳作为路径可以防止覆盖旧图
        File path = new File(c.getCacheDir() + File.separator + System.currentTimeMillis() + ".jpg");
        //通过 Context的getCacheDir()方法 获取文件存取的路径:getCacheDir()方法用于获取/data/data//cache目录
        // File.separator 作为跨平台的路径分隔符，保证在windows和linux，unix系统上都可以使用
        //用于获取/data/data/<application package>/cache目录
        // System.currentTimeMillis() :计算的是从1970年1月1日开始的时间，以毫秒为单位
        try {
            OutputStream os = new FileOutputStream(path);//输出流，向path处文件写入数据
            //  b.compress()方法用于压缩图片大小，包含参数(图片的压缩格式，图像压缩率，写入压缩数据的输出流)
            b.compress(Bitmap.CompressFormat.JPEG, 80, os);//quality表示压缩率
            os.close();//关闭资源
            return Uri.fromFile(path);//Creates a Uri from a file
        } catch (Exception ignored) {
        }
        return null;


}


}
